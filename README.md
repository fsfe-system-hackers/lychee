# Lychee

Lychee is the software we use to upload, organise and host pictures.
Internally, it's called the FSFE's picturebase.

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/lychee/00_README)
